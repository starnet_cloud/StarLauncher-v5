# ![logo](Png/Starlauncher_Login_Logo.gif)  (v5)

#### ———— 项目介绍


星登陆v5 | 已公开WEBUI界面部分，供用户自行修改为自己的界面，一个功能丰富且强力的网络启动器系统！

视频介绍： **https://www.bilibili.com/video/BV1YP411L7dC** 

星域互联：http://us.7skyun.com

淘宝：https://item.taobao.com/item.htm?id=561135852338

交流群号(QQ): 689363751

#### ———— 部署教程

 :fa-cloud: 视频教程： https://www.bilibili.com/video/BV1ov4y1T7K9

 :fa-cloud: 文档教程： https://gitee.com/starnet_cloud/StarLauncher-v5/wikis

#### ———— 功能说明

 :white_check_mark:   支持Minecraft版本: 全版本 (已经测试1.4.5 - 1.20.X 正常使用，甚至往后更新都可兼容)

 :white_check_mark:   软件一体化高度集成功能，丰富功能贯通无冲突 (整个基础系统当前包含已超过80个大+小功能)

 :white_check_mark:   启动器使用HTML5的WEBUI界面引擎，可自定义开发修改，极高自定义性

 :white_check_mark:   文件更新功能，支持多包组模式，支持分区独立更新，支持解压4G+压缩包

 :white_check_mark:   独立游戏多区功能，分区数据路径自定义(可用于数据隐藏在某些奇奇怪怪的文件夹)，支持完全自定义启动参数

 :white_check_mark:   已支持根据独立游戏区进行使用独立内置环境(JAVA)，单客户端集成多版本服主福音

 :white_check_mark:   玩家数据管理更详细且强大，支持邮箱，正版账户，QQ号码等绑定唯一数据，玩家可自助查看历史登录信息

 :white_check_mark:   用户验证分类功能支持详细开关单一类型，支持使用正则设置

 :white_check_mark:   集成完整世界树验证功能(Yggdrasil)，可支持第三方启动器登录验证，游戏皮肤等功能

 :white_check_mark:   集成目前通用皮肤处理功能(legacy/customskinload/uniskinman)，可自由调用接口

 :white_check_mark:   开放功能完善SDK接口支供开发者为星登陆系统拓展插件，拓展开发更多骚操作

 :white_check_mark:   新增在线交流功能，更方便随时协助玩家处理出现问题

 :white_square_button:  星登陆拓展插件市场，等待开发中

 :white_square_button:  星登陆拓展UI皮肤市场，等待开发中


 :speech_balloon:   **_如有功能建议和使用反馈请联系我们，提交的功能非常刚需可免费进行开发！_**  (2023年02月08日) ...



#### ———— 参与贡献


1.  星域互联信息团队 (本体开发)
2.  小埋 (UI开发)
3.  白熊 (配套开发)



#### ———— 星登陆开发历程


v1.0 - 2016年

v2.0 - 2017年

v3.0 - 2018年

v4.0 - 2020年

v5.0 - 2021年 . 07月 (开工)

v5.0 - 2022年 . 06月 (项目内测)

 _**v5.0 - 2022年 . 09月 (稳定公测使用)**_ 

#### ———— 软件部分界面显示


![星登陆运行结构](Png/%E6%98%9F%E7%99%BB%E9%99%86v5-%E8%BF%90%E8%A1%8C%E7%BB%93%E6%9E%84.png)
![控制台-文件更新](Png/image.png)
![控制台-系统设置部分](Png/image2.png)
![控制台-玩家列表](Png/image3.png)
![启动器-界面1](Png/image4.png)
![启动器-界面2](Png/image5.png)
![启动器-界面3](Png/image6.png)
![启动器-界面4](Png/image7.png)
![启动器-界面5](Png/image8.png)
![启动器-界面6](Png/image9.png)
![启动器-界面7](Png/image10.png)
![启动器-界面8](Png/image11.png)
![启动器-界面9](Png/image12.png)

本产品来自散装工程师编写.编写工具 ↓↓↓

![注-写代码工具](Png/d79293eba26dc3a3ed3c50a42d958e7.jpg)

———————————————————————————————————

唯有保持兴趣以及爱好才能走向永恒.